# tapestry3.0-hello-world

#### 介绍
简单例子。

#### 软件架构
Tapestry3.0


#### 安装教程

1. 编译只需tapestry3.0一个jar即可。
2. 运行时，需引入12个jar到`src/main/webapp/WEB-INF/lib`
3. 否则报错：缺少类
```
Caused by: java.lang.ClassNotFoundException: org.apache.commons.logging.Log
Caused by: java.lang.ClassNotFoundException: ognl.ClassResolver
java.lang.NoClassDefFoundError: org/apache/commons/fileupload/FileUploadException
java.lang.NoClassDefFoundError: org.apache.bsf.BSFManager
java.lang.NoClassDefFoundError: org/apache/commons/lang/builder/HashCodeBuilder
java.lang.ClassNotFoundException: org.apache.commons.digester.Digester
: org.apache.commons.collections.ArrayStack
: javassist.NotFoundException
等
```

#### 使用说明

1. 导入IntelliJ
2. 没有用maven，IntelliJ配置：
  - Output Path直接配置/Users/danni/work/svn/git/tapestry3.0-tutorial/tapestry3.0-hello-world/src/main/webapp/WEB-INF/classes
  - deployment配置webapp目录
3. 配置tomcat，启动tomcat
4. http://localhost:8080/app

> jdk我用1.8，tomcat用7.0.57和9.0.16都通过。
