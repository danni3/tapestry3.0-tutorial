<html><head><META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>JUnit Tests</title><link href="Tapestry.css" rel="stylesheet" type="text/css"><meta content="DocBook XSL Stylesheets V1.69.1" name="generator"><link rel="start" href="ContributorsGuide.html" title="Tapestry Contributor's Guide"><link rel="up" href="procedures.html" title="Chapter&nbsp;6.&nbsp;Development Procedures"><link rel="prev" href="procedures.deprecation.html" title="Deprecating methods and classes"><link rel="next" href="procedures.documentation.html" title="Documentation"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table summary="Navigation header" width="100%"><tr><th align="center" colspan="3">JUnit Tests</th></tr><tr><td align="left" width="20%"><a accesskey="p" href="procedures.deprecation.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><th align="center" width="60%">Chapter&nbsp;6.&nbsp;Development Procedures</th><td align="right" width="20%">&nbsp;<a accesskey="n" href="procedures.documentation.html"><img src="common-images/next.png" alt="Next"></a></td></tr></table><hr></div><div class="section" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="procedures.junit"></a>JUnit Tests</h2></div></div><div></div></div><p>
Tapestry has an excellent JUnit test suite, with code coverage
figures over 80% at the time of this writing (2.4-alpha-4).  It is
<span class="emphasis"><em>required</em></span> that changes to the framework
be accompanied by additional JUnit tests (typically, mock tests; see 
below) to validate the changes.  In addition, there is an ongoing
effort to fill in the gaps in the existing suite; the suite should reach
over 90% code coverage.
</p><p>
In order to compile and run the JUnit test suite you need to download 
<code class="literal">junit.jar</code> and <code class="literal">jdom-b8.jar</code>, 
and place them in the <code class="literal">ext-dist</code> directory. 
The official sites to download the libraries are listed in the README file
in that directory.
</p><p>
Some of the JUnit tests now require <a href="http://www.jython.org" target="_self">Jython</a>.  You must
download and install Jython 2.1,
then configure <code class="literal">jython.dir</code> in <code class="filename">config/build.properties</code>
to point to the install directory.  As usual, use an absolute path and forward slashes only.  To run
the JUnit test suite within Eclipse, you must
set the <code class="literal">JYTHON_DIR</code> classpath variable.
</p><p>
JUnit test source code is placed into the <code class="filename">junit/src</code> source tree.
The package name for JUnit tests is <code class="literal">org.apache.tapestry.junit</code>.
</p><p>
Less than half of Tapestry is tested using traditional JUnit tests.  The majority of JUnit testing occurs using
a system of mock unit tests.  Mock testing involves replacing the key classes of the
Servlet API (<code class="classname">HttpServletRequest</code>, <code class="classname">HttpSession</code>, etc.) with
out own implementations, with extensions that allow for checks and validations.  Instead of processing a series of
requests over HTTP, the requests are driven by an XML script file, which includes output checks.
</p><p>
Generally, each bit of functionality can be tested using its own mini-application.  
Create the application as
<code class="filename">junit/context<em class="replaceable"><code>X</code></em></code>.  This is much easier now, 
using Tapestry 3.0 features
such as dynamic lookup of specifications and implicit components.
</p><p>
The Mock Unit Test Suite is driven by scripts (whose structure is described below).  The suite
searches the directory <code class="filename">junit/mock-scripts</code> for files with the ".xml" extension.
Each of these is expected to be a test script.  The order in which scripts are executed is
arbitrary; scripts (and JUnit tests in general) should never rely on any order of execution.
</p><p>
Test scripts are named <code class="filename">Test<em class="replaceable"><code>Name</code></em>.xml</code>.
</p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note"><tr><td valign="top" align="center" rowspan="2" width="25"><img alt="[Note]" src="common-images/note.png"></td><th align="left">Note</th></tr><tr><td valign="top" align="left"><p>
The XML script is not validated, and invalid elements are
generally ignored.  The class <code class="classname">MockTester</code>
performs the test, and its capabilities are in fluxx, with
new capabilities being added as needed.
</p></td></tr></table></div><p>
A test script consists of an <code class="sgmltag-starttag">&lt;mock-test&gt;</code>
element.  Within it, the virtual context and servlet are defined.
</p><pre class="programlisting">

&lt;mock-test&gt;
  &lt;context name="c6" root="context6"/&gt;

  &lt;servlet name="app" class="org.apache.tapestry.ApplicationServlet"&gt;
    &lt;init-parameter name="org.apache.tapestry.engine-class"
       value="org.apache.tapestry.junit.mock.c6.C6Engine"/&gt;
  &lt;/servlet&gt;

</pre><p>
The name for the context becomes the leading term in any
generated URLs.  Likewise, the servlet name becomes the second
term.  The above example will generate URLs that reference
<code class="literal">/c6/app</code>.  Specifying a <code class="literal">root</code>
for a context identifies the root context directory (beneath the top level
<code class="filename">junit</code> directory).  In this example, HTML templates
go in <code class="filename">context6</code> and specifications
go in <code class="filename">context6/WEB-INF</code>.
</p><p>
Following the <code class="sgmltag-starttag">&lt;servlet&gt;</code>
and <code class="sgmltag-starttag">&lt;context&gt;</code> elements, a series
of <code class="sgmltag-starttag">&lt;request&gt;</code> elements.  Each such element
simulates a request.  A request specifies any query parameters passed as part
of the request, and contains a number of assertions that test
either the results, generally in terms of searching for strings
or regular expressions within the HTML response.
</p><pre class="programlisting">

&lt;request&gt;
  &lt;parameter name="service" value="direct"/&gt;
  &lt;parameter name="context" value="0/Home/$DirectLink"/&gt;
  
  &lt;assert-output name="Page Title"&gt;
&lt;![CDATA[
&lt;title&gt;Persistant Page Property&lt;/title&gt;
]]&gt;
  &lt;/assert-output&gt;

</pre><div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Warning"><tr><td valign="top" align="center" rowspan="2" width="25"><img alt="[Warning]" src="common-images/warning.png"></td><th align="left">Warning</th></tr><tr><td valign="top" align="left"><p>
As in the above example, it is very important
that HTML tags be properly escaped with the XML CDATA construct.
</p></td></tr></table></div><p>
Adding <code class="literal">failover="true"</code> to the
<code class="sgmltag-starttag">&lt;request&gt;</code> simulates a failover.  The contents
of the <code class="classname">HttpSession</code> are serialized,
then deserialized.  This ensures that all the data stored into the 
<code class="classname">HttpSession</code> will survive a failover to a new server within
a cluster.
</p><p>
All of the assertion elements expect a <code class="literal">name</code>
attribute, which is incorporated into any error message
if the assertion fails (that is, if the expected output
is not present).
</p><p>
The <code class="sgmltag-starttag">&lt;assert-output&gt;</code> element checks for the
presence of the contained literal output, contained within the
element.  Leading and trailing whitespace is trimmed before
the check is made.
</p><pre class="programlisting">

  &lt;assert name="Session Attribute"&gt;
  request.session.getAttribute("app/Home/message").equals("Changed")
  &lt;/assert&gt;				

</pre><p>
The <code class="sgmltag-starttag">&lt;assert&gt;</code> element checks
that the provided OGNL expression evaluates to true.
</p><pre class="programlisting">

  &lt;assert-regexp name="Error Message"&gt;
&lt;![CDATA[
&lt;span class="error"&gt;\s*You must enter a value for Last Name\.\s*&lt;/span&gt;
]]&gt;		
  &lt;/assert-regexp&gt; 

</pre><p>
The <code class="sgmltag-starttag">&lt;assert-regexp&gt;</code> looks
for a regular expression in the result, instead of a simple literal
string.
</p><pre class="programlisting">

  &lt;assert-output-matches name="Selected Radio" subgroup="1"&gt;
&lt;![CDATA[
&lt;input type="radio" name="inputSex" checked="checked" value="(.*?)"/&gt;
]]&gt;
    &lt;match&gt;1&lt;/match&gt;
  &lt;/assert-output-matches&gt;

</pre><p>
The <code class="sgmltag-starttag">&lt;assert-output-matches&gt;</code>
is the most complicated assertion.  It contains a regular expression
which is evaluated.  For each match, the subgroup value is extracted,
and compared to the next <code class="sgmltag-starttag">&lt;match&gt;</code>
value.  Also, the count of matches (vs. the number of
<code class="sgmltag-stattag">match</code> elements) is checked.
</p><pre class="programlisting">

  &lt;assert-output-stream name="Asset Content"
    content-type="image/gif"
    path="foo/bar/baz.gif"/&gt;

</pre><p>
The <code class="sgmltag-starttag">&lt;assert-output-stream&gt;</code>
element is used to compare the entire response to a static file
(this is normally associated with private assets).  A content type
must be specified, as well as a relative path to a file to compare
against.  The path is relative to the junit directory.  The
response must match the specified content type and actual content.
</p><pre class="programlisting">

  &lt;assert-exception name="Exception"&gt;
  File foo not found.
  &lt;/assert-exception&gt;

</pre><p>
The <code class="sgmltag-starttag">&lt;assert-exception&gt;</code> element
is used to check when an request fails entirely (is unable to send back a
response).  This only occurs when the application specification contains invalid
data (such as an incorrect class for the engine), or when the Exception page
is unable to execute.  The body of the element is matched against the exception's 
message property.
</p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Force a failure, then check for correctness"><tr><td valign="top" align="center" rowspan="2" width="25"><img alt="[Note]" src="common-images/note.png"></td><th align="left">Force a failure, then check for correctness</th></tr><tr><td valign="top" align="left"><p>
Sometimes the tests themselves have bugs.  A useful technique is
to purposely break the test to ensure that it is checking for what
it should check, then fix the test.  For example, adding <code class="literal">XXX</code>
into a <code class="sgmltag-starttag">&lt;assert-output&gt;</code>.  Run the test suite
and expect a failure, then remove the <code class="literal">XXX</code> and
re-run the test, which should succeed.
</p></td></tr></table></div></div><div class="navfooter"><hr><table summary="Navigation footer" width="100%"><tr><td align="left" width="40%"><a accesskey="p" href="procedures.deprecation.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><td align="center" width="20%"><a accesskey="u" href="procedures.html"><img src="common-images/up.png" alt="Up"></a></td><td align="right" width="40%">&nbsp;<a accesskey="n" href="procedures.documentation.html"><img src="common-images/next.png" alt="Next"></a></td></tr><tr><td valign="top" align="left" width="40%">Deprecating methods and classes&nbsp;</td><td align="center" width="20%"><a accesskey="h" href="ContributorsGuide.html"><img src="common-images/home.png" alt="Home"></a></td><td valign="top" align="right" width="40%">&nbsp;Documentation</td></tr></table></div></body></html>