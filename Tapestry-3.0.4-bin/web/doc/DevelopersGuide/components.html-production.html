<html><head><META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>Tapestry and HTML Production</title><link href="Tapestry.css" rel="stylesheet" type="text/css"><meta content="DocBook XSL Stylesheets V1.69.1" name="generator"><link rel="start" href="DevelopersGuide.html" title="Tapestry Developer's Guide"><link rel="up" href="components.html" title="Chapter&nbsp;3.&nbsp;Tapestry Components"><link rel="prev" href="components.html-template.html" title="HTML Templates"><link rel="next" href="components.localization.html" title="Localization"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table summary="Navigation header" width="100%"><tr><th align="center" colspan="3">Tapestry and HTML Production</th></tr><tr><td align="left" width="20%"><a accesskey="p" href="components.html-template.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><th align="center" width="60%">Chapter&nbsp;3.&nbsp;Tapestry Components</th><td align="right" width="20%">&nbsp;<a accesskey="n" href="components.localization.html"><img src="common-images/next.png" alt="Next"></a></td></tr></table><hr></div><div class="section" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="components.html-production"></a>Tapestry and HTML Production</h2></div></div><div></div></div><p>
Tapestry is design to work in a large-scale environment, that typically features
two seperate teams: a "creative" team that produces HTML and a 
"technical" team that produces Tapestry pages, components and Java code.
</p><p>
The division of skills is such that the creative team has virtually no knowledge of Java and
a minimal understanding of Tapestry, and the technical team has a limited understanding of HTML 
(and tend to be color blind).
</p><p>
The typical workflow is that the technical team implements the application, using very minimal HTML 
... that is, minimal attention to layout, font size, colors, etc.  Just enough to be sure that
the functionality of the application is there.
</p><p>
Meanwhile, the creative team is producing HTML pages of what the finished application will look like.  
These pages are like snapshots of the HTML produced by the running application.
</p><p>
        <span class="emphasis"><em>Integration</em></span> is the process of merging these two views of the application together.
Primarily, this involves marking up tags within the HTML page with 
<code class="varname">jwcid</code> attributes, 
to indicate
to Tapestry which portions of the page are dynamic.  In this way, the 
page can be used as a Tapestry HTML template.  These changes are designed to be invisible to a
<span class="acronym">WYSIWYG</span> HTML editor.
</p><p>
Tapestry includes a number of additional features to allow the HTML producers to continue
working on HTML templates, <span class="emphasis"><em>even after</em></span> their initial efforts have been
integrated with the Java developer's code.
</p><div class="section" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="N10542"></a>Implicitly removed bodies</h3></div></div><div></div></div><p>
In many cases, a component doesn't allow a body, but one may be present in the HTML template.
As usual, this is declared in
the <a href="spec.component-specification.html" title="<component-specification> element">component's specification</a>.
Tapestry considers that body to be a sample value, one which exists to allow the HTML producer
to verify the layout of the page using a WYSIWYG editor (rather than having to run the entire application).  
Tapestry simply edits out the body at runtime.
</p><p>For example, an HTML producer may create an HTML template that includes a table
cell to display the user's name.  The producer includes a sample value so that the
cell isn't empty (when previewing the HTML layout).
</p><div class="informalexample"><pre class="programlisting">
&lt;td&gt;&lt;span jwcid="insertName"&gt;John Doe&lt;/span&gt;&lt;/td&gt;
</pre></div><p>
The <a href="../ComponentReference/Insert.html" target="_self"><code class="classname">Insert</code></a> component doesn't allow a body, so Tapestry edits out the 
content of the <code class="sgmltag-starttag">&lt;span&gt;</code> tag from the HTML template.  The fact that
a <code class="sgmltag-starttag">&lt;span&gt;</code> was used to represent the <a href="../ComponentReference/Insert.html" target="_self"><code class="classname">Insert</code></a> component in the
HTML template is irrelevant to Tapestry; any tag could have been used, Tapestry just
cares that the start and end tags balance.
</p><p>
At runtime, Tapestry will combine the HTML template and the <a href="../ComponentReference/Insert.html" target="_self"><code class="classname">Insert</code></a> component to produce the
final HTML:
</p><div class="informalexample"><pre class="programlisting">
&lt;td&gt;Frank N. Furter&lt;/td&gt;
</pre></div><p>
This editting out isn't limited to simple text; any HTML inside the body is removed.  However,
none of that content may be dynamic ... the presence of a
<code class="varname">jwcid</code> attribute will cause a parsing exception.
</p></div><div class="section" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="N10575"></a>Explicitly removed bodies</h3></div></div><div></div></div><p>
Another feature related to production and integration is the ability to remove sections of the HTML template.
Producers often include some optional portions on the page.  The canonical example of this is a page that 
shows a table of results; the HTML producer will usually include extra rows to demonstrate the look and layout of
a fully populated page.
</p><p>
The first row will be wrapped by a <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a> and otherwise changed to include dynamic links and output, but what about
the other rows?
</p><p>
To handle this case,
Tapestry recognizes a special <code class="varname">jwcid</code> attribute value: <code class="literal">$remove$</code>.  
Using this special id causes
Tapestry to edit out the tag and all of its contents.  Thus, each additional <code class="sgmltag-starttag">&lt;tr&gt;</code> in the
table should specify the value <code class="literal">$remove$</code> for attribute <code class="varname">jwcid</code>.
</p><div class="informalexample"><pre class="programlisting">
&lt;table&gt;
  &lt;tr jwcid="foreach"&gt;
    &lt;td&gt;&lt;span jwcid="insertUserName"&gt;John Doe&lt;/span&gt;&lt;/td&gt;
    &lt;td&gt;&lt;span jwcid="insertAge"&gt;42&lt;/span&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr jwcid="$remove$"&gt;
  	&lt;td&gt;Frank N. Furter&lt;/td&gt;
  	&lt;td&gt;47&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr jwcid="$remove$"&gt;
    &lt;td&gt;Bob Doyle&lt;/td&gt;
    &lt;td&gt;24&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;</pre></div></div><div class="section" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="N10599"></a>Limiting template content
	</h3></div></div><div></div></div><p>
In a typical Tapestry application, some form of Border component provides a significant portion of every page.
This typically includes the outermost <code class="sgmltag-starttag">&lt;html&gt;</code>, <code class="sgmltag-starttag">&lt;head&gt;</code> and <code class="sgmltag-starttag">&lt;body&gt;</code>
tags, as well as <code class="sgmltag-starttag">&lt;table&gt;</code>s used to control layout.
</p><p>
In the static HTML pages from the creative team, this is not directly visible ... they <span class="emphasis"><em>must</em></span>
include all the content
normally generated by the Border component in order to see what the HTML page actually looks like.
</p><p>
By default, the <span class="emphasis"><em>entire</em></span> HTML template is the content for the page.  
This causes a problem, even after a <code class="sgmltag-starttag">&lt;span&gt;</code>
is added, to represent the Border component ... much of the HTML is duplicated, 
once from the static HTML, then dynamically from the Border component.
</p><p>
To eliminate this problem, Tapestry has a second special <code class="varname">jwcid</code> attribute: <code class="literal">$content$</code>.
Using this special id causes Tapestry to limit its view of the HTML template to just the content inside the tag.  Anything outside
the defined content is completely ignored.
</p></div><div class="section" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="N105C5"></a>Limits</h3></div></div><div></div></div><p>
Ideally, the HTML pages created by the HTML producers would be used as is
as the HTML templates.  Changes made for integration, the adding of <code class="varname">jwcid</code> attributes and such,
would be copied back into the HTML pages.
</p><p>
Given the use of the <code class="literal">$remove$</code> and <code class="literal">$content$</code>
          <code class="varname">jwcid</code>'s, 
this is practical
to a point.  Once the application starts using a number of re-usable components, there
isn't a good way to perform the integration short of cutting and replacing
some of the HTML page content to form the HTML template.
</p></div></div><div class="navfooter"><hr><table summary="Navigation footer" width="100%"><tr><td align="left" width="40%"><a accesskey="p" href="components.html-template.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><td align="center" width="20%"><a accesskey="u" href="components.html"><img src="common-images/up.png" alt="Up"></a></td><td align="right" width="40%">&nbsp;<a accesskey="n" href="components.localization.html"><img src="common-images/next.png" alt="Next"></a></td></tr><tr><td valign="top" align="left" width="40%">HTML Templates&nbsp;</td><td align="center" width="20%"><a accesskey="h" href="DevelopersGuide.html"><img src="common-images/home.png" alt="Home"></a></td><td valign="top" align="right" width="40%">&nbsp;Localization</td></tr></table></div></body></html>