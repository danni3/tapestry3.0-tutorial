<html><head><META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>Dynamic Page State</title><link href="Tapestry.css" rel="stylesheet" type="text/css"><meta content="DocBook XSL Stylesheets V1.69.1" name="generator"><link rel="start" href="DevelopersGuide.html" title="Tapestry Developer's Guide"><link rel="up" href="pages.html" title="Chapter&nbsp;4.&nbsp;Tapestry Pages"><link rel="prev" href="pages.ejb-props.html" title="EJB Page Properties"><link rel="next" href="pages.stale-links.html" title="Stale Links and the Browser Back Button"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table summary="Navigation header" width="100%"><tr><th align="center" colspan="3">Dynamic Page State</th></tr><tr><td align="left" width="20%"><a accesskey="p" href="pages.ejb-props.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><th align="center" width="60%">Chapter&nbsp;4.&nbsp;Tapestry Pages</th><td align="right" width="20%">&nbsp;<a accesskey="n" href="pages.stale-links.html"><img src="common-images/next.png" alt="Next"></a></td></tr></table><hr></div><div class="section" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="pages.dynamic-state"></a>Dynamic Page State</h2></div></div><div></div></div><p>
The properties of a page and components on the page can change during the rendering process.   
These are changes to the page's dynamic state.
</p><p>
The majority of components in an application use their bindings to pull data from the page (or 
from business objects reachable from the page).
</p><p>
A small number of components, notably the <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a> component, work the other way; pushing 
data back to the page (or some other component).
</p><p>
The <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a> component is used to loop over 
a set of items.  It has one parameter from which it 
reads the list of items.  A second parameter is used to write each item back to a property of its 
container.
</p><p>
For example, in our shopping cart example, we may use a <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a> to run 
through the list of line 
items in the shopping cart.  Each line item identifies the product, cost and quantity.
</p><div class="example"><a name="N107BB"></a><p class="title"><b>Example&nbsp;4.1.&nbsp;HTML template for Shopping Cart</b></p><pre class="programlisting">
&lt;h1&gt;Context of shopping cart for
&lt;span jwcid="insertUserName"&gt;John Doe&lt;/span&gt;&lt;/h1&gt;
&lt;table&gt;
  &lt;tr&gt;
    &lt;th&gt;Product&lt;/th&gt; &lt;th&gt;Qty&lt;/th&gt; &lt;th&gt;Price&lt;/th&gt;
  &lt;/tr&gt;
  &lt;span jwcid="eachItem"&gt;
  &lt;tr&gt;
    &lt;td&gt;&lt;span jwcid="insertProductName"&gt;Product Name&lt;/span&gt;&lt;/td&gt;
    &lt;td&gt;&lt;span jwcid="insertQuantity"&gt;5&lt;/span&gt;&lt;/td&gt;
    &lt;td&gt;&lt;span jwcid="insertPrice"&gt;$1.50&lt;/span&gt;&lt;/td&gt;
    &lt;td&gt;&lt;a jwcid="remove"&gt;remove&lt;/a&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;/span&gt;
&lt;/table&gt;</pre></div><p>
This example shows a reasonable template, including sample static values used
when previewing the HTML layout (they are removed by Tapestry at runtime).  Some
areas have been glossed over, such as allowing quantities to be changed.
</p><p>
Component <code class="varname">eachItem</code> is our <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a>.   
It will render its body (all the text and components it wraps) several times, 
depending on the number of line items in the cart.  On each pass it:
</p><div class="itemizedlist"><ul type="disc"><li><p>Gets the next value from the source</p></li><li><p>Updates the value into some property of its container</p></li><li><p>Renders its body</p></li></ul></div><p>
This continues until there are no more values in its source.  Lets say this is a page that has a 
<code class="varname">lineItem</code> property that is being updated by the 
<code class="varname">eachItem</code> component.  The <code class="varname">insertProductName</code>, 
<code class="varname">insertQuantity</code> and <code class="varname">insertPrice</code> components use dynamic 
bindings such as <code class="literal">lineItem.productName</code>, 
<code class="literal">lineItem.quantity</code> and <code class="literal">lineItem.price</code>.
</p><p>
Part of the page's specification would configure these embedded components.
</p><div class="example"><a name="N107F6"></a><p class="title"><b>Example&nbsp;4.2.&nbsp;Shopping Cart Specification (excerpt)</b></p><pre class="programlisting">
&lt;component id="eachItem" type="<a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a>"&gt;
  &lt;binding name="source" expression="items"/&gt;
  &lt;binding name="value" expression="lineItem"/&gt;
&lt;/component&gt;

&lt;component id="insertProductName type="<a href="../ComponentReference/Insert.html" target="_self"><code class="classname">Insert</code></a>"&gt;
  &lt;binding name="value" expression="lineItem.productName"/&gt;
&lt;/component&gt;

&lt;component id="insertQuantity" type="<a href="../ComponentReference/Insert.html" target="_self"><code class="classname">Insert</code></a>"&gt;
  &lt;binding name="value" expression="lineItem.quantity"/&gt;
&lt;/component&gt;

&lt;component id="insertPrice" type="<a href="../ComponentReference/Insert.html" target="_self"><code class="classname">Insert</code></a>"&gt;
  &lt;binding name="value" expression="lineItem.price"/&gt;
&lt;/component&gt;

&lt;component id="remove" type="<a href="../ComponentReference/ActionLink.html" target="_self"><code class="classname">ActionLink</code></a>"&gt;
  &lt;binding name="listener" expression="listeners.removeItem"/&gt;
&lt;/component&gt;
</pre></div><p>
This is very important to the <code class="varname">remove</code> component.  On some future request cycle, it will be 
expected to remove a specific line item from the shopping cart, but how will it know which one?
</p><p>
This is at the heart of the <a href="cycle.action.html" title="Action service">action service</a>.  One aspect of the 
<a href="../api/org/apache/tapestry/IRequestCycle.html" target="_self"><code class="classname">IRequestCycle</code></a>'s functionality is to 
dole out a sequence of action ids that are used for this purpose (they are also involved in forms 
and form elements).  As the <a href="../ComponentReference/ActionLink.html" target="_self"><code class="classname">ActionLink</code></a> component renders itself, 
it allocates the next action id from 
the request cycle.  Regardless of what path through the page's component hierarchy the rendering 
takes, the numbers are doled out in sequence.  This includes conditional blocks and loops such as 
the <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a>.
</p><p>
The steps taken to render an HTML response are very deterministic.  If it were possible to 
'rewind the clock' and restore all the involved objects back to the same state (the same values for 
their instance variables) that they were just before the rendering took place, the end result would 
be the same.  The exact same HTML response would be created.
</p><p>
This is similar to the way in which compiling a program from source code results in the same object 
code.  Because the inputs are the same, the results will be identical.
</p><p>
This fact is exploited by the action service to respond to the URL.  In fact, the state of the page 
and components <span class="emphasis"><em>is</em></span> rolled back and the rendering processes fired again (with output discarded).  
The <a href="../ComponentReference/ActionLink.html" target="_self"><code class="classname">ActionLink</code></a> component can compare the action id against the target action id encoded 
within the URL.  When a match is found, the <a href="../ComponentReference/ActionLink.html" target="_self"><code class="classname">ActionLink</code></a> component can count on the state of the 
page and all components on the page to be in the exact same state they were in when the page 
was previously rendered.
</p><p>
A small effort is required of the developer to always ensure that this rewind operation works.  In 
cases where this can't be guaranteed (for instance, if the source of this dynamic data is a stock 
ticker or unpredictable database query) then other options must be used, including the use of
the <a href="../ComponentReference/ListEdit.html" target="_self"><code class="classname">ListEdit</code></a> component.
</p><p>
In our example, the <code class="varname">remove</code> component would trigger some application specific code 
implemented in its containing page that removes the current <code class="varname">lineItem</code> from the shopping cart.
</p><p>
The application is responsible for providing a
<a href="cycle.listeners.html" title="Action and Direct listeners">listener method</a>, a method which is invoked
when the link is triggered.
</p><div class="example"><a name="N10857"></a><p class="title"><b>Example&nbsp;4.3.&nbsp;Listener method for remove component</b></p><pre class="programlisting">
public void removeItem(IRequestCycle cycle)
{
  getCart().remove(lineItem);
}
</pre></div><p>
This method is only invoked after all the page state is rewound; 
especially relevant is the <code class="varname">lineItem</code> property.  
The listener gets the shopping cart and removes the current line item from it.
This whole rewinding process has ensured that <code class="varname">lineItem</code> is the correct value, even though the remove 
component was rendered several times on the page (because it was wrapped by the <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a>
component).
</p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Listener Methods vs. Listener Objects"><tr><td valign="top" align="center" rowspan="2" width="25"><img alt="[Note]" src="common-images/note.png"></td><th align="left">Listener Methods vs. Listener Objects</th></tr><tr><td valign="top" align="left"><p>
	<a href="cycle.listeners.html" title="Action and Direct listeners">Listener methods</a> were introduced in Tapestry 1.0.2.  Prior to that, it was necessary
	to create a listener object, typically as an inner class, to be notified when
	the link or form was triggered.  This worked against the basic goal of Tapestry: to 
	eliminate or simplify coding.  In reality, the listener objects are still there,
	they are created automatically and use Java reflection to invoke the
	correct listener method.
	</p></td></tr></table></div><p>
An equivalent JavaServer Pages application would have needed to define a servlet for removing 
items from the cart, and would have had to encode in the URL some identifier for the item to be 
removed.  The servlet would have to pick apart the URL to find the cart item identifier, locate the 
shopping cart object (probably stored in the <code class="classname">HttpSession</code>)
and the particular item and invoke 
the <code class="function">remove()</code> method directly.  Finally, it would forward to the JSP that would produce the 
updated page.
</p><p>
The page containing the shopping cart would need to have special knowledge of the cart 
modifying servlet; its servlet prefix and the structure of the URL (that is, how the item to remove 
is identified).  This creates a tight coupling between any page that wants to display the shopping 
cart and the servlet used to modify the shopping cart.  If the shopping cart servlet is modified 
such that the URL it expects changes structure, all pages referencing the servlet will be broken. 
</p><p>
Tapestry eliminates all of these issues, reducing the issue of manipulating the shopping cart down 
to the single, small listener method.  
</p></div><div class="navfooter"><hr><table summary="Navigation footer" width="100%"><tr><td align="left" width="40%"><a accesskey="p" href="pages.ejb-props.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><td align="center" width="20%"><a accesskey="u" href="pages.html"><img src="common-images/up.png" alt="Up"></a></td><td align="right" width="40%">&nbsp;<a accesskey="n" href="pages.stale-links.html"><img src="common-images/next.png" alt="Next"></a></td></tr><tr><td valign="top" align="left" width="40%">EJB Page Properties&nbsp;</td><td align="center" width="20%"><a accesskey="h" href="DevelopersGuide.html"><img src="common-images/home.png" alt="Home"></a></td><td valign="top" align="right" width="40%">&nbsp;Stale Links and the Browser Back Button</td></tr></table></div></body></html>