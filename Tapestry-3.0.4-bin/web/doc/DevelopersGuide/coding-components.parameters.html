<html><head><META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>Parameters and Bindings</title><link href="Tapestry.css" rel="stylesheet" type="text/css"><meta content="DocBook XSL Stylesheets V1.69.1" name="generator"><link rel="start" href="DevelopersGuide.html" title="Tapestry Developer's Guide"><link rel="up" href="coding-components.html" title="Chapter&nbsp;9.&nbsp;Designing new components"><link rel="prev" href="coding-components.base-class.html" title="Choosing a base class"><link rel="next" href="coding-components.persistent-state.html" title="Persistent Component State"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table summary="Navigation header" width="100%"><tr><th align="center" colspan="3">Parameters and Bindings</th></tr><tr><td align="left" width="20%"><a accesskey="p" href="coding-components.base-class.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><th align="center" width="60%">Chapter&nbsp;9.&nbsp;Designing new components</th><td align="right" width="20%">&nbsp;<a accesskey="n" href="coding-components.persistent-state.html"><img src="common-images/next.png" alt="Next"></a></td></tr></table><hr></div><div class="section" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="coding-components.parameters"></a>Parameters and Bindings</h2></div></div><div></div></div><p>
You may create a component that has parameters.  Under Tapestry, component parameters are a 
kind of "named slot" that can be wired up as 
a source (or sink) of data in a number of ways.  This "wiring up" is 
actually accomplished using binding objects.
</p><div class="tip" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Tip: Connected Parameters"><tr><td valign="top" align="center" rowspan="2" width="25"><img alt="[Tip]" src="common-images/tip.png"></td><th align="left">Connected Parameters</th></tr><tr><td valign="top" align="left"><p>
Most components use "in" parameters and can have Tapestry
<a href="components.connected-params.html" title="Connected Parameters">connect the parameters to properties
of the component</a> automatically.  This discussion
reveals some inner workings of Tapestry
that developers most often
no longer need to be aware of.
</p></td></tr></table></div><p>
The page loader, the object that converts a component specification into an actual 
component, is responsible for creating and assigning the bindings.  It uses the method 
<code class="function">setBinding()</code> to assign a binding with a name.  Your 
component can retrieve the binding by name using <code class="function">getBinding()</code>.
</p><p>
For example, lets create a component that allows the color of 
a span of text to be specified using a <code class="classname">java.awt.Color</code> object.  The 
component has a required parameter named <code class="varname">color</code>.  The class's 
<code class="function">renderComponent()</code> method is below:
</p><div class="informalexample"><pre class="programlisting">
protected void renderComponent(<a href="../api/org/apache/tapestry/IMarkupWriter.html" target="_self"><code class="classname">IMarkupWriter</code></a> writer, <a href="../api/org/apache/tapestry/IRequestCycle.html" target="_self"><code class="classname">IRequestCycle</code></a> cycle)
  throws RequestCycleException
{
  <a href="../api/org/apache/tapestry/IBinding.html" target="_self"><code class="classname">IBinding</code></a> colorBinding = getBinding("color");
  Color color = (Color)colorBinding.getObject("color", Color.class);
  String encodedColor = <a href="../api/org/apache/tapestry/RequestContext.html" target="_self"><code class="classname">RequestContext</code></a>.encodeColor(color);

  writer.begin("font");
  writer.attribute("color", encodedColor);

  renderWrapped(writer, cycle);

  writer.end();
}</pre></div><p>
The call to <code class="function">getBinding()</code> is relatively expensive, since 
it involves rummaging around in a <code class="classname">Map</code> and then 
casting the result from <code class="classname">java.lang.Object</code> to <code class="classname">org.apache.tapestry.IBinding</code>.  
</p><p>
Because bindings are typically set once and then read frequently by the component, 
implementing them as private instance variables is much more efficient.  Tapestry 
allows for this as an optimization on frequently used components.
</p><p>
The <code class="function">setBinding()</code> method in 
<a href="../api/org/apache/tapestry/AbstractComponent.html" target="_self"><code class="classname">AbstractComponent</code></a> checks to see if there is a read/write 
JavaBeans property named "<em class="replaceable"><code>name</code></em>Binding" of
type <a href="../api/org/apache/tapestry/IBinding.html" target="_self"><code class="classname">IBinding</code></a>.  In this example, it 
would look for the methods <code class="function">getColorBinding()</code> and <code class="function">setColorBinding()</code>.
</p><p>
If the methods are found, they are invoked from 
<code class="function">getBinding()</code> and <code class="function">setBinding()</code> instead of updating the 
<code class="classname">Map</code>.
</p><p> This changes the example to:</p><div class="informalexample"><pre class="programlisting">
<span class="emphasis"><em>private <a href="../api/org/apache/tapestry/IBinding.html" target="_self"><code class="classname">IBinding</code></a> colorBinding;

public void setColorBinding(<a href="../api/org/apache/tapestry/IBinding.html" target="_self"><code class="classname">IBinding</code></a> value)
{
  colorBinding = value;
}

public <a href="../api/org/apache/tapestry/IBinding.html" target="_self"><code class="classname">IBinding</code></a> getColorBinding()
{
  return colorBinding;
}
</em></span>

protected void renderComponent(<a href="../api/org/apache/tapestry/IMarkupWriter.html" target="_self"><code class="classname">IMarkupWriter</code></a> writer, <a href="../api/org/apache/tapestry/IRequestCycle.html" target="_self"><code class="classname">IRequestCycle</code></a> cycle)
  throws RequestCycleException
{
  Color color = (Color)<span class="emphasis"><em>colorBinding</em></span>.getObject("color", Color.class);
  String encodedColor = <a href="../api/org/apache/tapestry/RequestContext.html" target="_self"><code class="classname">RequestContext</code></a>.encodeColor(color);

  writer.begin("font");
  writer.attribute("color", encodedColor);

  renderWrapped(writer, cycle);

  writer.end();
}</pre></div><p>
This is a trade off; slightly more code for slightly better performance.  
There is also a memory bonus; the 
<code class="classname">Map</code> used by <a href="../api/org/apache/tapestry/AbstractComponent.html" target="_self"><code class="classname">AbstractComponent</code></a> to store the binding will never be created.
</p></div><div class="navfooter"><hr><table summary="Navigation footer" width="100%"><tr><td align="left" width="40%"><a accesskey="p" href="coding-components.base-class.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><td align="center" width="20%"><a accesskey="u" href="coding-components.html"><img src="common-images/up.png" alt="Up"></a></td><td align="right" width="40%">&nbsp;<a accesskey="n" href="coding-components.persistent-state.html"><img src="common-images/next.png" alt="Next"></a></td></tr><tr><td valign="top" align="left" width="40%">Choosing a base class&nbsp;</td><td align="center" width="20%"><a accesskey="h" href="DevelopersGuide.html"><img src="common-images/home.png" alt="Home"></a></td><td valign="top" align="right" width="40%">&nbsp;Persistent Component State</td></tr></table></div></body></html>