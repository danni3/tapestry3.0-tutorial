<html><head><META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>HTML Templates</title><link href="Tapestry.css" rel="stylesheet" type="text/css"><meta content="DocBook XSL Stylesheets V1.69.1" name="generator"><link rel="start" href="DevelopersGuide.html" title="Tapestry Developer's Guide"><link rel="up" href="components.html" title="Chapter&nbsp;3.&nbsp;Tapestry Components"><link rel="prev" href="components.embedded.html" title="Embedded Components"><link rel="next" href="components.html-production.html" title="Tapestry and HTML Production"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table summary="Navigation header" width="100%"><tr><th align="center" colspan="3">HTML Templates</th></tr><tr><td align="left" width="20%"><a accesskey="p" href="components.embedded.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><th align="center" width="60%">Chapter&nbsp;3.&nbsp;Tapestry Components</th><td align="right" width="20%">&nbsp;<a accesskey="n" href="components.html-production.html"><img src="common-images/next.png" alt="Next"></a></td></tr></table><hr></div><div class="section" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="components.html-template"></a>HTML Templates</h2></div></div><div></div></div><p>Nearly all Tapestry components combine static HTML
	<sup>[<a href="#ftn.N10438" name="N10438">2</a>]</sup>
from a template with additional dynamic content 
(some few components are just dynamic content).  
Often, a Tapestry component embeds other Tapestry components.  
These inner components are referenced in the containing component's template.
</p><p>
One of the features of Tapestry is <span class="emphasis"><em>invisible instrumentation</em></span>.  In most
web application frameworks, converting a static HTML page into a usable template is a destructive process:
the addition of new tags, directives or even Java code to the template means that it will no
longer preview properly in a <span class="acronym">WYSIWYG</span> editor.
</p><p>
Tapestry templates are instrumented using a new HTML attribute, <code class="varname">jwcid</code>, to any existing
element.  Elements with such attributes are recognized by Tapestry as being dynamic, and driven by
a Tapestry component, but a <span class="acronym">WYSIWYG</span> editor will simply ignore them.  Once a template
is instrumented, it may be worked on by 
<a href="components.html-production.html" title="Tapestry and HTML Production">both the  HTML producer and the Java developer</a>.
</p><p>
Identifying a Tapestry component within
an HTML template 
is accomplished by adding a <code class="varname">jwcid</code> attribute to a tag.
</p><div class="informalexample"><pre class="programlisting">
&lt;<em class="replaceable"><code>any</code></em> jwcid="<em class="replaceable"><code>component id</code></em>" ... &gt;  <em class="replaceable"><code>body</code></em>  &lt;/<em class="replaceable"><code>any</code></em>&gt;
</pre><p>or
</p><pre class="programlisting">
&lt;<em class="replaceable"><code>any</code></em> jwcid="<em class="replaceable"><code>component id</code></em>" ... /&gt;
</pre></div><p>
Most often, the HTML element chosen is <code class="sgmltag-starttag">&lt;span&gt;</code>, though (in fact) Tapestry
completely ignores the element chosen by the developer, except to make sure the open and close tags balance.
</p><p>
The parser used by Tapestry is relatively forgiving about case 
and white space.  Also, the component id (and any other attributes) can be enclosed in 
double quotes (as above), single quotes, or be left unquoted.
</p><p>
You are free to specify additional
attributes.  These attributes will become
<a href="components.informal-parameters.html" title="Formal vs. Informal Parameters">informal parameters</a> for the Tapestry component.
</p><p>
The start and end tags for Tapestry components must balance properly. This includes cases where the
end tag is normally ommitted, such as <code class="sgmltag-starttag">&lt;input&gt;</code> elements.  Either a closing
tag must be supplied, or the XML-style syntax for an empty element must be used (that is, 
a slash just before the end of the tag).
</p><div class="section" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="component.html-template.localization"></a>Localizing sections of a template</h3></div></div><div></div></div><p>
Tapestry includes an additional template feature to assist with localization of a web application.
By specifying a <code class="sgmltag-starttag">&lt;span&gt;</code> element with a special attribute,
<code class="varname">key</code>, Tapestry will replace the entire 	
<code class="sgmltag-starttag">&lt;span&gt;</code> tag with a
<a href="components.localization.html#components.localization.strings" title="Localization with Strings">localized string</a> for the component.
</p><p>
This construct takes one of two forms:
</p><div class="informalexample"><pre class="programlisting">
&lt;span key="<em class="replaceable"><code>key</code></em>" ... &gt; ... &lt;/span&gt;
</pre><p>or</p><pre class="programlisting">
&lt;span key="<em class="replaceable"><code>key</code></em>" ... /&gt;
</pre></div><p>
If only the <code class="varname">key</code>
attribute is specified, then the <code class="sgmltag-starttag">&lt;span&gt;</code>
is simply replaced with the localized string.  However, if any additional attributes
are specified for the <code class="sgmltag-starttag">&lt;span&gt;</code> tag beyond
<code class="varname">key</code>, then
the <code class="sgmltag-starttag">&lt;span&gt;</code> tag will be part of the rendered HTML, with
the specified attributes.
</p><p>
The upshot of this is that sections of the HTML template can be invisibly localized
simply by wrapping the text to be replaced inside a <code class="sgmltag-starttag">&lt;span&gt;</code>
tag.  The wrapped text exists, once more, as sample text to be displayed
in a <span class="acronym">WYSIWYG</span> editor.
</p></div><div class="section" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="component.html-template.body"></a>Components with Bodies</h3></div></div><div></div></div><p>
In Tapestry, individual components may have their own HTML templates.   This is a very powerful
concept ... it allows powerful and useful components to be created with very little code.   By contrast,
accomplishing the same using JSP tags requires either that all the HTML be output
from the JSP tag directly, or that the JSP tag use some additional framework, such as Velocity,
to enable the use of a template.  In either case the JSP tag author will need to divide the code or template
into two pieces (before the body and after the body).  Tapestry allows components to simply have 
a single template, with a marker for where the body is placed.
</p><p>
During the rendering
of a page, Tapestry knits together the templates of the page and all the nested components to create
the HTML response sent back to the client web browser.
</p><div class="informalexample"><pre class="programlisting">
Container content <a name="component.html-template.body.container1"></a><img border="0" alt="1" src="standard-images/callouts/1.png">

&lt;span jwcid="component"&gt; <a name="component.html-template.body.component"></a><img border="0" alt="2" src="standard-images/callouts/2.png">

  Body content <a name="component.html-template.body.content"></a><img border="0" alt="3" src="standard-images/callouts/3.png">
  
&lt;/span&gt;

More container content <a name="component.html-template.body.container2"></a><img border="0" alt="4" src="standard-images/callouts/4.png">
</pre></div><div class="calloutlist"><table summary="Callout list" border="0"><tr><td align="left" valign="top" width="5%"><a href="#component.html-template.body.container1"><img border="0" alt="1" src="standard-images/callouts/1.png"></a> </td><td align="left" valign="top"><p>
	This portion of the container content is rendered first.
	</p></td></tr><tr><td align="left" valign="top" width="5%"><a href="#component.html-template.body.component"><img border="0" alt="2" src="standard-images/callouts/2.png"></a> </td><td align="left" valign="top"><p>
	The component is then rendered.  It will render, possibly using its
	own template.
	</p></td></tr><tr><td align="left" valign="top" width="5%"><a href="#component.html-template.body.content"><img border="0" alt="3" src="standard-images/callouts/3.png"></a> </td><td align="left" valign="top"><p>
	The component controls <span class="emphasis"><em>if</em></span>, <span class="emphasis"><em>when</em></span> and
	<span class="emphasis"><em>how often</em></span> the body content from its container
	is rendered.
	</p><p>
	Body content can be a mix of static HTML and additional components.  These
	components are <span class="emphasis"><em>wrapped</em></span> by the component, but are
	<span class="emphasis"><em>embedded</em></span> in the component's container.
	</p></td></tr><tr><td align="left" valign="top" width="5%"><a href="#component.html-template.body.container2"><img border="0" alt="4" src="standard-images/callouts/4.png"></a> </td><td align="left" valign="top"><p>
	After the component finishes rendering, the remaining content
	from the container is rendered.
	</p></td></tr></table></div><p>
The body listed above can be either static HTML or other Tapestry 
components or both.  Elements in the body of a component are 
wrapped by the containing component.  The containing component controls the 
rendering of the elements it wraps in its body.  For example, 
the <a href="../ComponentReference/Conditional.html" target="_self"><code class="classname">Conditional</code></a> component may decide not to 
render its body and the <a href="../ComponentReference/Foreach.html" target="_self"><code class="classname">Foreach</code></a> component may render 
its body multiple times.
</p><p>
Not all Tapestry components should have a body.  
For example, the <a href="../ComponentReference/TextField.html" target="_self"><code class="classname">TextField</code></a> component creates an
<code class="sgmltag-starttag">&lt;input type=text&gt;</code>
form element and it makes no sense for it to contain anything else.  
Whether a component allows a body (and wrap other elements), or whether
it discards it, is defined in the 
<a href="spec.component-specification.html" title="<component-specification> element">component's specification</a>.
</p><p>
Tapestry includes a special component, <a href="../ComponentReference/RenderBody.html" target="_self"><code class="classname">RenderBody</code></a>, 
which is used to render the body content from a component's container.  
It makes it easy to create components that wrap other components.
</p></div><div class="footnotes"><br><hr align="left" width="100"><div class="footnote"><p><sup>[<a href="#N10438" name="ftn.N10438">2</a>] </sup>
			The current releases of Tapestry is specifically oriented around HTML.  Some support for
			non-HTML languages, such as XML, XHTML or WML is already present
			and will be expanded in the future.
		</p></div></div></div><div class="navfooter"><hr><table summary="Navigation footer" width="100%"><tr><td align="left" width="40%"><a accesskey="p" href="components.embedded.html"><img src="common-images/prev.png" alt="Prev"></a>&nbsp;</td><td align="center" width="20%"><a accesskey="u" href="components.html"><img src="common-images/up.png" alt="Up"></a></td><td align="right" width="40%">&nbsp;<a accesskey="n" href="components.html-production.html"><img src="common-images/next.png" alt="Next"></a></td></tr><tr><td valign="top" align="left" width="40%">Embedded Components&nbsp;</td><td align="center" width="20%"><a accesskey="h" href="DevelopersGuide.html"><img src="common-images/home.png" alt="Home"></a></td><td valign="top" align="right" width="40%">&nbsp;Tapestry and HTML Production</td></tr></table></div></body></html>